// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

window._ = require('lodash')

import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'

require('./assets/sass/style.sass');

import { library, config } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'

library.add(fas)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  mounted() {
    let vm = this
    if (localStorage.users != undefined) {
      if (localStorage.users == '') {
        localStorage.removeItem('users')
      } else {
        vm.$store.state.users = JSON.parse(localStorage.users)
      }
    } else {
      localStorage.removeItem('users')
    }

    if (localStorage.products != undefined) {
      if (localStorage.products == '') {
        localStorage.removeItem('products')
      } else {
        vm.$store.state.products = JSON.parse(localStorage.products)
      }
    } else {
      localStorage.removeItem('products')
    }

    if (localStorage.user_data != undefined) {
      if (localStorage.user_data == '') {
        localStorage.removeItem('user_data')
      } else {
        vm.$store.state.user_data = JSON.parse(localStorage.user_data)
      }
    } else {
      localStorage.removeItem('user_data')
    }

    if (localStorage.is_admin != undefined) {
      if (localStorage.is_admin == '') {
        localStorage.removeItem('is_admin')
      } else {
        vm.$store.commit('setAdmin', localStorage.is_admin)
      }
    } else {
      localStorage.removeItem('is_admin')
    }

    if (localStorage.authenticated != undefined) {
      if (localStorage.authenticated == '') {
        localStorage.removeItem('authenticated')
      } else {
        vm.$store.commit('setAuthentication', localStorage.authenticated)
      }
    } else {
      localStorage.removeItem('authenticated')
    }
  }
})
