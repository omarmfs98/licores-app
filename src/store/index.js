import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const createStore = () => {
  return new Vuex.Store({
    state: () => ({
      authenticated: false,
      products: [],
      user_data: null,
      users: [
        { id: 1, name: 'Admin', user: 'admin', email: 'admin@beer.com', password: '12345', type: 'ADMIN' }
      ],
      is_admin: false,
      type_error: ''
    }),
    mutations: {
      setAuthentication (state, res) {
        state.authenticated = res
      },
      setAdmin (state, res) {
        state.is_admin = res
      },
      products (state, res) {
        state.products = res
      },
      setUserData (state, res) {
        state.user_data = res
      },
      addNewUser (state, res) {
        state.users.push(res)
      },
      addNewProduct (state, res) {
        state.products.push(res)
      },
      deleterUser (state, res) {
        let del = state.users
        _.remove(del, function (u) {
          return u.id == res
        })
        state.users = del

      },
      deleterProduct (state, res) {
        let del = state.products
        _.remove(del, function (u) {
          return u.id == res
        })
        state.products = del
      },
      validateUser (state, res) {
        let user = _.find(state.users, function (u) {
          return u.user == res.user
        })

        let email = _.find(state.users, function (u) {
          return u.email == res.email
        })

        if (user) {
          state.type_error = 'USERNAME'
        } else if (email) {
          state.type_error = 'EMAIL'
        } else {
          state.users.push(res)
        }
      },
      logout(state) {
        state.authenticated = false
        if (state.is_admin) {
          state.is_admin = false
        }
      },
      validate(state, res) {
        let auth = _.find(state.users, function (u) {
          return res.user == u.user && res.password == u.password
        })

        if (auth) {
          state.authenticated = true
          state.user_data = auth
          localStorage.setItem("user_data", JSON.stringify(state.user_data))
          if (auth.type == 'ADMIN') {
            state.is_admin = true
          }
        }
      }
    }
  })
}

export default createStore
