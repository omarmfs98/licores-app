import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/HelloWorld'
import Login from '@/components/auth/Login'
import Register from '@/components/auth/Register'
import Products from '@/components/products/Index'
import Users from '@/components/users/Index'
import CreateUser from '@/components/users/Create'
import CreateProduct from '@/components/products/Create'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Pagina principal',
      component: Index
    },
    {
      path: '/login',
      name: 'Inicio de sesión',
      component: Login
    },
    {
      path: '/register',
      name: 'Registro',
      component: Register
    },
    {
      path: '/products',
      name: 'Productos',
      component: Products
    },
    {
      path: '/users',
      name: 'Usuarios',
      component: Users
    },
    {
      path: '/users/create',
      name: 'Crear Usuario',
      component: CreateUser
    },
    {
      path: '/products/create',
      name: 'Crear Producto',
      component: CreateProduct
    }
  ]
})
